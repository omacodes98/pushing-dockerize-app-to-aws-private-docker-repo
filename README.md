# Pushing Dockerized App Container to registry 

- Dockerized Nodejs application and pushed to private Docker registry 

## Table of Contents
- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Write Dockerfile to build a Docker image for a Nodejs application 

* Create private Docker registry on AWS(AmazonECR)

* Push Docker image to this private repository

## Technologies Used

* Docker 

* Node.js 

* Amazon ECR

## Steps 

step 1: Create Docker repo on AmazonECR

[Created ECR repo](/images/01_Created_Docker_repo_AmazonECR.png) 

Step 2: Install AWS client 

    curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"


Step 3: Log into AWS docker repo

     docker aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 522116691231.dkr.ecr.eu-west-2.amazonaws.com

[Logged in AWS docker repo](/images/02_logged_in_AWS_docker_repo.png)

Step 4: Tag image 

    docker tag my-app:1.0 522116691231.dkr.ecr.eu-west.amazonaws.com/my-app:1.0

[Tag image](/images/03_tagging_images.png)

Step 5: Push docker image to AWS 

    docker push 522116691231.dkr.ecr.eu-west-2.amazonaws.com/my-app:1.0

[Pushing Docker image to aws repo](/images/04_pushing_docker_image_to_AWS_docker_repo.png)

[Image in AWS docker repo UI](/images/05_image_in_aws_docker_repo_UI.png)

Step 6: Change something in the code

[Change something in code](/images/06_changes_to_code.png)

Step 7: Build new version of image

    docker build -t my-app:1.1 .

[Build new version of Image](/images/07_building_new_version_of_image.png)

Step 8: Tag new Image version 

    docker tag my-app:1.1 522116691231.dkr.ecr.eu-west.amazonaws.com/my-app:1.1

[Tagging new image version](/images/08_tagging_new_image_version.png)

Step 9: push new image verion to AWS 

    docker push 522116691231.dkr.ecr.eu-west-2.amazonaws.com/my-app:1.1

[Push new image version](/images/09_pushed_new_image_version.png)

[AWS Docker repo UI](/images/10_AWS_Docker_repo_UI.png)

## Installation

* curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"

* sudo installer -pkg AWSCLIV2.pkg -target /


## Usage 

Run $ node server.js 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/pushing-dockerize-app-to-aws-private-docker-repo.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/pushing-dockerize-app-to-aws-private-docker-repo

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.